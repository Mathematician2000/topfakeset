# TopFakeSet: A Dataset for Fake News Classification task

A dataset of news published in Russian.

Contains 57k news tagged as 'real' or one of the following types of fake:
- satire
- commentary
- persuasive information
- citizen journalism


This project contains the following files and directories:
+ Crawlers/ &ndash; a directory with crawlers used to collect the news for TopFakeSet dataset
+ Partial data/ &ndash; a directory containing dataframes with news from different sources (in .csv format)
+ Server/ &ndash; a directory with Flask app set up on heroku
+ dataset.csv &ndash; a dataframe with preprocessed news (TopFakeSet)
+ raw_dataset.csv &ndash; a dataframe with raw news


More info on https://top-fake-set.herokuapp.com