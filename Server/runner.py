import os

from flask_script import Manager, Server, Shell
from waitress import serve

from app import app


def make_shell_context():
    return {
        'app': app,
    }


PORT = int(os.environ.get('PORT', 33507))

manager = Manager(app)
manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('runserver', Server(host='0.0.0.0', port=PORT))


@manager.option('-h', '--host', help='Server host', default='0.0.0.0')
@manager.option('-p', '--port', help='Server port', default=None)
def runprodserver(host, port):
    if port is None:
        port = PORT
    serve(app, host=host, port=port)


if __name__ == '__main__':
    manager.run()
