import os

from flask import Flask

import config
from .classifier import FakeNewsModel


app = Flask(__name__)
app.config.from_object(os.environ.get('FLASK_ENV', 'config.TestingConfig'))

model = FakeNewsModel()


from . import views
