from flask import render_template

from app import app, model
from .forms import ClassifyTextForm


@app.route('/')
def page_home():
    return render_template('home.html')


@app.route('/what_is_fake/')
def page_fakes():
    return render_template('what_is_fake.html')


@app.route('/classify/', methods=['POST', 'GET'])
def page_classify():
    result = ""
    form = ClassifyTextForm()
    if form.validate_on_submit():
        text = form.text.data
        result = model.classify(text)
    return render_template('classify.html', form=form, result=result)


@app.route('/about/')
def page_about():
    return render_template('about.html')
