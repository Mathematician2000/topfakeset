import pickle
from warnings import filterwarnings

from mlxtend.classifier import EnsembleVoteClassifier
from xgboost import Booster, XGBClassifier

from .tokenizer import Tokenizer


filterwarnings('ignore')
RANDOM_STATE = 42


class FakeNewsModel():
    '''Classifier class, containing 3 voting models, vectorizer & tokenizer'''

    def __init__(self):
        self._vectorizer = pickle.load(open('app/static/tfidf_vect.sav', 'rb'))
        self._vectorizer.tokenizer = lambda x: x
        self._vectorizer.lowercase = False
        self._tokenizer = Tokenizer()
        self.class_labels = [
            'real_news',
            'citizen_journalism',
            'satire',
            'commentary',
            'persuasive_information',
        ]

        with open('app/static/reduced_mlp_model.pkl', 'rb') as fin:
            reduced_mlp_model = pickle.load(fin)
        with open('app/static/tfidf_logreg_model.pkl', 'rb') as fin:
            tfidf_logreg_model = pickle.load(fin)

        booster = Booster()
        booster.load_model('app/static/tfidf_boost_model.bin')
        tfidf_boost_model = XGBClassifier(random_state=RANDOM_STATE,
                                          objective='multi:softprob',
                                          num_class=len(self.class_labels),
                                          verbosity=0)
        tfidf_boost_model._Booster = booster

        self._model = EnsembleVoteClassifier(clfs=[
                                                 tfidf_boost_model,
                                                 reduced_mlp_model,
                                                 tfidf_logreg_model,
                                             ],
                                             voting='soft',
                                             fit_base_estimators=False) \
                      .fit(None, [x for x in range(len(self.class_labels))])

    def classify(self, text):
        tokenized_text = self._tokenizer(text)
        tfidf = self._vectorizer.transform([tokenized_text])
        pred, = self._model.predict(tfidf)
        return self.class_labels[pred]
