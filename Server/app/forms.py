from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField
from wtforms.validators import DataRequired


class ClassifyTextForm(FlaskForm):
    text = TextAreaField("Введите текст новости для классификации",
                         validators=[DataRequired()])
    submit = SubmitField("Запустить!")
