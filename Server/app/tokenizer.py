# -*- coding: utf-8 -*-


import re

from pymorphy2 import MorphAnalyzer


class Tokenizer():
    '''Tokenizer class to prepare text before constructing TF-IDF vector'''

    def __init__(self):
        self.morph = MorphAnalyzer()

    def clean_text(self, text):
        # newsru.com
        if text.startswith('"'):
            text = text[1:]
        if text.endswith('"'):
            text = text[:-1]
        elif text.endswith('".'):
            text = text[:-2] + '.'

        # interfax.ru
        string = 'INTERFAX.RU - '
        L = len(string)
        idx = text.find(string)
        if idx != -1:
            text = text[idx + L:]

        # ria.ru
        patterns = [
            r'^ *\w+, \d+ \w+ — РИА Новости, \w+ \w+\. ',
            r'^ *\w+, \d+ \w+ — РИА Новости\. ',
            r'Мнение автора может не совпадать с позицией редакции\.?$',
        ]
        for pattern in patterns:
            text = re.sub(pattern, '', text, count=0, flags=re.I)

        # meduza.io
        patterns = [
            r'Фото в анонсе.*\nНапишите нам$',
        ]
        for pattern in patterns:
            text = re.sub(pattern, '', text, count=0, flags=re.I)

        # lenta.ru
        abc = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        ABC = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
        re_date = rf'\d\d:\d\d — +\d+ [{abc}]+( \d\d\d\d)?'

        pattern = (
            rf'Материалы по теме({re_date}«?[{ABC}].*?[^{ABC} ]'
            rf'«?[{ABC}].*?)+[^{ABC} ]«?[{ABC}]'
        )
        m = re.search(pattern, text)
        if m is not None:
            text = text[:m.start()] + '\n' + text[m.end() - 1:]
        bad_re = rf'[^-{ABC} \n«"\'\(][{ABC}]'
        m = re.search(bad_re, text)
        if m is not None:
            text = text[:m.start() + 1] + '\n' + text[m.end() - 1:]
            m = re.search(bad_re, text)

        return text

    def symbols_tokenize(self, text):
        text = text.replace('ё', 'е').replace('Ё', 'Е').replace('…', '...')
        for c in '“‘':
            text = text.replace(c, '«')
        for c in '”′ʼ’ʺ':
            text = text.replace(c, '»')
        for c in '—-–‑―−‒ー─‐':
            text = text.replace(c, '-')

        m = re.search(r'\w( +-|- +)\w', text)
        while m is not None:
            i, j = m.start(), m.end()
            text = text[:i] + text[i:j].replace(' ', '') + text[j:]
            m = re.search(r'\w( +-|- +)\w', text)

        m = re.search(r'"[^ ]+"', text)
        while m is not None:
            i, j = m.start(), m.end()
            text = text[:i] + '«' + text[i + 1:j - 1] + '»' + text[j:]
            m = re.search(r'"[^ ]+"', text)

        m = re.search(r"'[^ ]+'", text)
        while m is not None:
            i, j = m.start(), m.end()
            text = text[:i] + '«' + text[i + 1:j - 1] + '»' + text[j:]
            m = re.search(r"'[^ ]+'", text)

        abc = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
        ABC = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
        m = re.search(rf'[{abc}\.\?!»][{ABC}]', text)
        while m is not None:
            idx = m.start() + 1
            text = text[:idx] + '\n' + text[idx + 1:]
            m = re.search(rf'[{abc}\.\?!][{ABC}]', text)

        return text

    def handle_datetime(self, text):
        re_min_or_sec = r'([0-5]\d|0[6-9])'
        re_hour = r'(0|1)\d?|2[0-3]'
        re_time = rf'({re_hour})(:{re_min_or_sec}){{1,2}}'
        re_0_day = r'([0-2]\d|3[01])'
        re_0_month = r'(0\d|1[12])'
        re_day = r'([0-2]?\d|3[01])'
        re_month = r'(0?\d|1[12])'
        re_eng_month = (
            r'(jan(uary)?|feb(ruary)?|mar(ch)?|apr(il)?|may|'
            r'jun(e)?|jul(y)?|aug(ust)?|sep(tember)?|oct(ober)?|'
            r'nov(ember)?|dec(ember)?)'
        )
        re_rus_month = (
            r'(январ(ь|я)|феврал(ь|я)|март(а)|апрел(ь|я)|ма(й|я)|'
            r'июн(ь|я)|июл(ь|я)|август(а)|сентяьр(ь|я)|октябр(ь|я)|'
            r'ноябр(ь|я)|декабр(ь|я))'
        )

        date_patterns = {
            'yyyy(-|по)yyyy': r'\d{4}( *(по|-|до) *)\d{4}',

            '[d]d[-го] ru_mm[ yyyy[ года]][ в hh:mm[:ss]]': (
                rf'{re_day}(-?о?(е|го))? +{re_rus_month}'
                rf'( +\d{4}( +(года|г\.?))?)?( +(с|к|в|на|,)? +{re_time})?'
            ),
            '[d]d[-th] en_mm[ yyyy][ at hh::mm[::ss]]': (
                rf'{re_day}(-?th)? +(of +)?{re_eng_month}'
                rf'( +\d{4})?( +at +{re_time})?'
            ),

            'yyyy-mm[-dd]': rf'\d{{4}}-{re_0_month}(-{re_0_day})?',
            'dd-mm-yyyy': rf'{re_0_day}-{re_0_month}-\d{{4}}',

            '[h]h:mm[:ss]': re_time,

            '[m]m/[d]d/[yy]yy': rf'{re_month}/{re_day}/(\d\d){{1,2}}',
            'dd.mm.[yy]yy': rf'{re_0_day}\.{re_0_month}\.(\d\d){{1,2}}',
        }

        for pattern in date_patterns.values():
            text = re.sub(pattern, ' <DATETIME> ', text)
        return text

    def special_tokenize(self, text):
        re_digit_symbols = r'⅔⅓¾½¼³²₂₅₁₀\%/\$№€#₽£\*\+×∼℃º°→\^∞÷'
        re_integer = r'\d[ \'\d]*'
        re_float = rf'{re_integer}[,\./]{re_integer}'

        patterns = {  # fixed order!
            '<EMOJI>': (
                r'[￼🎊🥶❄️🔦🏢🛋👁♻🌿👫🐉😎⚔💊💋🎤👨🏻👩🏼🎨⏳🎙💳✈🛂🚑🚃📱🏨🍔🎭🛍'
                r'🚗🛳🚄😊ツ❤💪📸✊⚽😅✪⭐🦇🔴🖤🍊🌊😂🔥❗‍♂🇷🇺🥊🇸📝🦁👉⚡🤯💻✨🌃⚪💍'
                r'🐐🆚🇹😨☺⚫🎉✌👏🔵😀💖🇧👻☝👍🤔🗣👋💛❌📋📲🍾🎄🎁🧑🦲😭✔😃😉⁦⁩📹'
                r'🎾😡💉🗓💚🚨🖥🤤🇯🇵🏠⏱Ә💥ù🙌🎥🤘💯🤲🙅🤣📷👑🖋⁣🤗✅🤠♦💗🤸🙃🤍🧝🏽🥳'
                r'🐾🙏😬👎̈💣🔫😍💧🌸💐♬🤴👸👶👊💙🦋🤎💃☀🐘⚜]'
            ),
            '<URL>': (
                r'(https? ?: ?//)?[a-zA-Z0-9\./\?:@_=#-]+\.[a-zA-Z]{2,6}'
                r'[a-zA-Z0-9\.&%/\?:@_=#-]*'
            ),
            '<NUM>': (
                rf'[{re_digit_symbols} ]*({re_float}|{re_integer})'
                rf'[{re_digit_symbols} ]*'
            ),
            '<UTF>': (
                r'[óíêý­ ́̆öקלפיèáūōü§ÓĘ·™@Å„®à저는오래전부터서울을구경하고싶었습니다안녕세요'
                r'어디에버스표를살수있까복궁은매우아름답도시의망보기좋곳이입감사합정말맛징가움직여드레'
                r'즈소주몇건물비행접와같생겼ç얼굴바르크림추천해한국식라떼트Ēé\ƒÉņāžīļķēšú Šôä'
                r'~ії‌¸⁃•►`â¯|●こんにちはў ÜÁ‎µřò➡îćã✍⃣ңүөҡӨҙәғҠһҺҫ▼▲․¡ЄėℹåІ▫ÚÇ'
                r'موقعالحترفين☘ìģ𝙎𝙪𝙯𝙮𝘾𝙤𝙧𝙩𝙚єΠοδσφαιρτήςηΑΕΚΝάμνΣκλώόεγέńυθίπ'
                r'ωßčΟΜÍᴢᴀʜʙɪÀÖ©♀ñşَأُّهذِآْكإٌبسطدۖۚىؤłęχą∙ıČÆβ法维拉韦ů𝒮𝓊𝓃𝓈𝑒𝓉𝓇𝒸𝒽ğ⁰'
                r'øðï𝐶𝑎𝑚𝑖𝑙ЎЈ]'
            ),
        }

        for token, pattern in patterns.items():
            text = re.sub(pattern, f' {token} ', text)
        return text

    def handle_punctuation(self, text):
        punct = '.,!?:();«»'
        strange_symbols = [
            c for c in text
            if not (c.isalnum() or
                    c in punct or
                    c.isspace() or
                    c in '<>-')
        ]
        for c in punct:
            text = text.replace(c, f' {c} ')
        for c in strange_symbols:
            text = text.replace(c, '')
        text = text.replace('. . .', '...')

        return text

    def remove_spaces(self, text):
        for s in ['\r', '\n\n']:
            text = text.replace(s, '\n')
        for c in ['	⁠', ' ', ' ', '\t', '⠀']:
            text = text.replace(c, ' ')
        text = re.sub(r'  +', ' ', text)
        return text.strip()

    def normalize_text(self, text):
        return list(map(lambda word: self.morph.parse(word)[0].normal_form,
                        text.split()))

    def __call__(self, text):
        return self.normalize_text(
            self.remove_spaces(
                self.handle_punctuation(
                    self.special_tokenize(
                        self.handle_datetime(
                            self.symbols_tokenize(
                                self.clean_text(text)))))))
